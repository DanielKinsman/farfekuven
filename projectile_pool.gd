class_name ProjectilePool
extends Node3D

const ProjectileScene := preload("res://projectile.tscn")
const POOL_SIZE := 20

@onready var next_index := 0


func _ready():
    assert(top_level)
    for i in range(POOL_SIZE):
        add_child(ProjectileScene.instantiate())


func next() -> Projectile:
    var projectile: Projectile = get_child(next_index)
    next_index = wrapi(next_index + 1, 0, self.get_child_count())
    return projectile


func fire(origin_transform, origin_velocity):
    var projectile := next()
    projectile.position = origin_transform * Vector3.FORWARD * 1.0
    projectile.visual.position = projectile.position
    projectile.linear_velocity = origin_velocity + (origin_transform.basis * Projectile.PROJECTILE_VELOCITY)
    projectile.visible = get_parent().visible
    projectile.process_mode = get_parent().process_mode

    if projectile.is_multiplayer_authority():
        projectile.replicate_fire.rpc(projectile.position, projectile.linear_velocity)
