class_name Player
extends CharacterBody3D


const MOTION_SPEED := 1.5
const JUMP_SPEED := 3.75
const GROUND_FRICTION := 0.75
const AIR_FRICTION := 1.0
const MOUSE_SMOOTHING := 0.25

var previous_position: Vector3
var previous_velocity: Vector3
var last_input_frame_processed_by_authority := 0
var last_authority_frame := 0
var input_latency_frames := 0

var peer_id: int:
    set(value):
        peer_id = value
        if State.trust_clients:
            get_node("State").set_multiplayer_authority(peer_id)

        get_node("Inputs").set_multiplayer_authority(peer_id)

@onready var state: PlayerState = $State
@onready var inputs: PlayerInputs = $Inputs
@onready var visual: Node3D = $Visual
@onready var head: MeshInstance3D = $Visual/MeshInstance3DHead
@onready var camera: Camera3D = $Visual/MeshInstance3DHead/Camera3D
@onready var projectile_pool: ProjectilePool = $ProjectilePool
@onready var target_pitch_yaw := Vector2.ZERO
@onready var pitch_yaw := Vector2.ZERO
@onready var first_input_frame := 0
@onready var frames_offset_count := 0
@onready var physics_buffer: TickIndexedBuffer = null


func _ready():
    if inputs.is_multiplayer_authority():
        camera.current = true
        head.visible = false
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

    if is_multiplayer_authority():
        physics_buffer = TickIndexedBuffer.new(Engine.physics_ticks_per_second * 2)


func _process(delta: float):
    visual.position = visual.position.lerp(position, Engine.get_physics_interpolation_fraction())

    if not inputs.is_multiplayer_authority():
        return

    pitch_yaw.x = lerp_angle(pitch_yaw.x, target_pitch_yaw.x, MOUSE_SMOOTHING)
    pitch_yaw.y = lerp_angle(pitch_yaw.y, target_pitch_yaw.y, MOUSE_SMOOTHING)
    visual.rotation = Vector3.ZERO
    head.rotation = Vector3.ZERO
    visual.rotate_y(pitch_yaw.y)
    head.rotate_x(pitch_yaw.x)


func _physics_process(delta: float):
    if not State.contacted_by_server:
        return

    previous_position = position
    previous_velocity = velocity

    if inputs.is_multiplayer_authority():
        inputs.update(is_on_floor(), Vector3(head.rotation.x, visual.rotation.y, 0.0), state.authority_physics_frame)

    if state.is_multiplayer_authority():
        physics_process_authority(delta)   # Do authoritative physics calculation
    elif inputs.is_multiplayer_authority():
        physics_process_input_authority(delta)  # Do client prediction
    else:
        # This is on the client side and self is a remote player
        if state.authority_physics_frame > last_authority_frame:
            # make sure we don't process older out of order packets or the same packet twice
            last_authority_frame = state.authority_physics_frame

            # Show other clients in the past, don't do prediction for them, use velocity for interpolation
            position = position.lerp(state.position, State.server_lerp_rate)
            velocity = state.velocity
            visual.rotation = Vector3(0.0, state.rotation.y, 0.0)
            head.rotation = Vector3(state.rotation.x, 0.0, 0.0)


func _unhandled_input(event):
    if (inputs.is_multiplayer_authority()
            and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and event is InputEventMouseMotion):
        mouse_look(event)


func physics_process_authority(delta: float):
    assert(is_multiplayer_authority())
    if first_input_frame <= 0:
        first_input_frame = inputs.input_buffer.get_min_frame()

    if inputs.input_buffer.max_frame <= 0:
        return

    if inputs.input_buffer.max_frame - first_input_frame < Settings.client_input_frame_buffer_size:
        return  # buffer the first few inputs

    if last_input_frame_processed_by_authority == 0:
        last_input_frame_processed_by_authority = inputs.input_buffer.max_frame - (Settings.client_input_frame_buffer_size + 1)
        assert(last_input_frame_processed_by_authority > 0)

    var input_frame: int = last_input_frame_processed_by_authority + 1

    # If buffer read head is consistently off where it should be, we're probably just offset with our matching of
    # client frame to server frame. Catch it up.
    var ideal_read_head_position: int = inputs.input_buffer.max_frame - Settings.client_input_frame_buffer_size
    if input_frame < ideal_read_head_position:
        frames_offset_count = 0 if frames_offset_count > 0 else frames_offset_count - 1
    if input_frame > ideal_read_head_position:
        frames_offset_count = 0 if frames_offset_count < 0 else frames_offset_count + 1
    else:
        frames_offset_count = 0

    if absi(frames_offset_count) > Engine.physics_ticks_per_second:
        print_debug("frame mismatch for %s frames, adjusting buffer read head" % frames_offset_count)
        input_frame -= signi(frames_offset_count)

    var motion: Vector3
    var actions: int
    var rot: Vector3

    var inp: Variant = inputs.input_buffer.get_data(input_frame)
    if inp == null:
        if Settings.client_input_frame_buffer_size > 0:
            print_debug("input frame %s missing from buffer, max frame in buffer %s" %
                [input_frame, inputs.input_buffer.max_frame])

        motion = inputs.motion
        actions = 0  # don't repeat stale actions
        visual.rotation = Vector3(0.0, state.rotation.y, 0.0)
        head.rotation = Vector3(state.rotation.x, 0.0, 0.0)
    else:
        motion = inp[0]
        actions = inp[1]
        visual.rotation = Vector3(0.0, inp[2].y, 0.0)
        head.rotation = Vector3(inp[2].x, 0.0, 0.0)

    move(delta, motion, actions, is_on_floor())
    act(actions)

    state.update(
        mini(input_frame, inputs.input_buffer.max_frame), # don't send updates from the client's future
        position,
        velocity,
        Vector3(head.rotation.x, visual.rotation.y, 0.0),
    )
    last_input_frame_processed_by_authority = input_frame
    physics_buffer.add(Engine.get_physics_frames(), [position, velocity])

    # TODO when trusting client if inputs.is_multiplayer_authority(): do something


func physics_process_input_authority(delta: float):
    assert(inputs.is_multiplayer_authority())
    if state.input_physics_frame > 0 and state.input_physics_frame >= last_input_frame_processed_by_authority:
        last_input_frame_processed_by_authority = state.input_physics_frame
        input_latency_frames = Engine.get_physics_frames() - state.input_physics_frame
        assert(input_latency_frames >= 0, "measured latency should never be negative")
        # Want less latency? Turn off vsync :(
        # https://github.com/godotengine/godot-proposals/issues/2020#issuecomment-1093824271

        # Attempt to rewind and replay inputs since `state.input_physics_frame` even though
        # godot can't do this yet https://github.com/godotengine/godot-proposals/issues/2821
        assert(previous_position == position)
        position = state.position
        velocity = state.velocity

        for frame in range(state.input_physics_frame + 1, Engine.get_physics_frames()):
            var inp: Variant = inputs.input_buffer.get_data(frame)
            if inp == null:
                print_debug("client side input buffer empty! %s" % Engine.get_physics_frames())
            else:
                move(delta, inp[0], inp[1], inp[2])  # assumes constant delta!

    if (position - previous_position).length_squared() > 0.05 or (velocity - previous_velocity).length_squared() > 0.05:
        print_debug("client side prediction error %s, position %s vs %s, velocity %s vs %s" % [
            Engine.get_physics_frames(), previous_position, position, previous_velocity, velocity])

    move(delta, inputs.motion, inputs.actions, is_on_floor())
    act(inputs.actions)


func mouse_look(event: InputEventMouseMotion):
    # credit https://github.com/crazyStewie/godot_simple_first_person_controller/blob/master/assets/player/Player.gd
    var pitch_yaw := Vector2(-event.relative.y if Settings.mouse_invert_pitch else event.relative.y, event.relative.x)
    target_pitch_yaw -= pitch_yaw * Settings.mouse_sensitivity * PI / 180.0
    target_pitch_yaw.x = clampf(target_pitch_yaw.x, -PI / 2.0, PI / 2.0)
    target_pitch_yaw.y = wrapf(target_pitch_yaw.y, -PI, PI)


func get_gravity() -> Vector3:
    var space = get_viewport().find_world_3d().space
    return (PhysicsServer3D.area_get_param(space, PhysicsServer3D.AREA_PARAM_GRAVITY) *
        PhysicsServer3D.area_get_param(space, PhysicsServer3D.AREA_PARAM_GRAVITY_VECTOR))


func move(delta : float, input_motion: Vector3, actions: int, grounded: bool):
    velocity += get_gravity() * delta
    var friction := AIR_FRICTION

    if grounded:
        velocity += input_motion * MOTION_SPEED
        friction *= GROUND_FRICTION
        if actions & PlayerInputs.JUMP:
            velocity.y += JUMP_SPEED

    # TODO make friciton dependent on delta
    velocity.x *= friction
    velocity.y *= AIR_FRICTION
    velocity.z *= friction
    move_and_slide()


func act(actions):
    # jump handled in move()
    if actions & PlayerInputs.FIRE:
        if inputs.is_multiplayer_authority():
            projectile_pool.fire(head.global_transform, velocity)  # client side prediction

        if is_multiplayer_authority():
            # latency compensation:
            # send client's server tick along with input
            # assert we are on the server
            # if client's server tick > current server tick, client is cheating
            if inputs.authority_frame > Engine.get_physics_frames():
                print("Client is cheating!")
                return

            # halve difference between client's server tick and current server tick
            var one_way_trip_time := int(ceil((Engine.get_physics_frames() - inputs.authority_frame) / 2.0))

            # find player position and velocity at that time
            var fire_frame = Engine.get_physics_frames() - one_way_trip_time
            var past = physics_buffer.get_data(fire_frame, [head.global_transform, velocity])
            # TODO get basis direct from player input at firing time rather than old direction player was facing on
            # the server.
            var head_transform: Transform3D = Transform3D(head.global_transform.basis, past[0] + head.position)
            projectile_pool.fire(head_transform, past[1])
            # TODO resim and check for all hits between "then" and "now
