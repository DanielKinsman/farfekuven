class_name PlayerSpawner
extends MultiplayerSpawner

const PlayerScene = preload("res://player.tscn")

func _spawn_custom(data):
    var player := PlayerScene.instantiate()
    player.peer_id = data
    print("spawned player for peer %s" % player.peer_id)
    return player
