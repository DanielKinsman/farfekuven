class_name GameState
extends Node


var upnp: UPNP = null
var upnp_discover_thread: Thread = null

@export var trust_clients := false
@export var server_lerp_rate := 0.99

@onready var contacted_by_server := false


func _ready():
    multiplayer.peer_connected.connect(_player_connected)
    multiplayer.peer_disconnected.connect(_player_disconnected)
    Engine.physics_ticks_per_second = Settings.ticks_per_second
    # sync network to _physics_process rather than _process
    get_tree().multiplayer_poll = false


func _physics_process(_delta):
    multiplayer.poll()


func host():
    var peer = ENetMultiplayerPeer.new()
    peer.create_server(Settings.server_port)
    multiplayer.set_multiplayer_peer(peer)
    contacted_by_server = true

    if Settings.upnp_enabled:
        upnp_discover_thread = Thread.new()
        upnp_discover_thread.start(upnp_forward)


func upnp_forward():
    if upnp == null:
        upnp = UPNP.new()

    upnp.discover()

    if upnp.get_gateway() and upnp.get_gateway().is_valid_gateway():
        var result = upnp.add_port_mapping(
            Settings.server_port,
            0,
            ProjectSettings.get_setting("application/config/name"),
            "UDP",
        )
        assert(result == UPNP.UPNP_RESULT_SUCCESS)


func upnp_unforward():
    if upnp != null:
        upnp.delete_port_mapping(Settings.server_port, "UDP")


func join(address: String, port: int):
    var peer = ENetMultiplayerPeer.new()
    peer.create_client(address, port)
    multiplayer.set_multiplayer_peer(peer)


func _player_connected(id: int):
    print("player id %s connected" % id)
    if is_multiplayer_authority():
        apply_settings_from_server.rpc_id(id, Engine.physics_ticks_per_second)


func _player_disconnected(id: int):
    get_tree().quit()


@rpc(reliable)
func apply_settings_from_server(ticks_per_second: int):
    Engine.physics_ticks_per_second = ticks_per_second
    contacted_by_server = true


func _unhandled_input(event):
    if event.is_action_pressed("ui_cancel"):
        get_tree().quit()


func _exit_tree():
    upnp_unforward()
    if upnp_discover_thread != null:
        upnp_discover_thread.wait_to_finish()
        upnp_discover_thread = null
