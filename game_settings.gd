class_name GameSettings
extends Node


const DEFAULT_CONFIG_PATH := "user://settings.cfg"
const DEFAULT_PORT := 58361
const DEFAULT_TICKS_PER_SECOND := 60
const DEFAULT_CLIENT_INPUT_FRAME_BUFFER_SIZE := 1
const DEFAULT_UPNP_ENABLED := true

const DEFAULT_WINDOW_MODE := DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN

const DEFAULT_MOUSE_INVERT_PITCH := false
const DEFAULT_MOUSE_SENSITIVITY := 0.25


var server_port: int:
    get:
        return _config.get_value("server", "port")
    set(value):
        _config.set_value("server", "port", value)


var ticks_per_second: int:
    get:
        return _config.get_value("server", "ticks_per_second")
    set(value):
        _config.set_value("server", "ticks_per_second", value)


var client_input_frame_buffer_size: int:
    get:
        return _config.get_value("server", "client_input_frame_buffer_size")
    set(value):
        _config.set_value("server", "client_input_frame_buffer_size", value)


var upnp_enabled: bool:
    get:
        return _config.get_value("server", "upnp_enabled")
    set(value):
        _config.set_value("server", "upnp_enabled", value)


var window_mode: DisplayServer.WindowMode:
    get:
        return _config.get_value("display", "window_mode")
    set(value):
        _config.set_value("display", "window_mode", value)


var vsync_mode: DisplayServer.VSyncMode:
    get:
        return _config.get_value("display", "vsync_mode")
    set(value):
        _config.set_value("display", "vsync_mode", value)


var fps_limit: int:
    get:
        return _config.get_value("rendering", "fps_limit")
    set(value):
        _config.set_value("rendering", "fps_limit", value)


var mouse_invert_pitch: bool:
    get:
        return _config.get_value("input", "mouse_invert_pitch")
    set(value):
        _config.set_value("input", "mouse_invert_pitch", value)


var mouse_sensitivity: float:
    get:
        return _config.get_value("input", "mouse_sensitivity")
    set(value):
        _config.set_value("input", "mouse_sensitivity", value)


@onready var _config := ConfigFile.new()


func _ready():
    load_defaults()
    load_settings()
    apply_display_settings()


func apply_display_settings():
    DisplayServer.window_set_mode(window_mode)
    DisplayServer.window_set_vsync_mode(vsync_mode)
    if DisplayServer.window_get_mode() in [DisplayServer.WINDOW_MODE_FULLSCREEN,
            DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN]:
        # godot 4 alpha 6 seems to ned this :shrug:
        DisplayServer.window_set_size(DisplayServer.screen_get_size())

    Engine.target_fps = fps_limit


func save_settings(path: String = DEFAULT_CONFIG_PATH):
    _config.save(path)


func load_settings(path: String = DEFAULT_CONFIG_PATH):
    var err := _config.load(path)
    if err != OK:
        save_settings(path)


func load_defaults():
    server_port = DEFAULT_PORT
    ticks_per_second = DEFAULT_TICKS_PER_SECOND
    client_input_frame_buffer_size = DEFAULT_CLIENT_INPUT_FRAME_BUFFER_SIZE
    upnp_enabled = DEFAULT_UPNP_ENABLED
    window_mode = DEFAULT_WINDOW_MODE
    mouse_invert_pitch = DEFAULT_MOUSE_INVERT_PITCH
    mouse_sensitivity = DEFAULT_MOUSE_SENSITIVITY

    var refresh := DisplayServer.screen_get_refresh_rate()
    if refresh > 0.0:
        vsync_mode = DisplayServer.VSYNC_MAILBOX
        fps_limit = int(ceil(refresh)) * 2  # limit to 2x refresh rate
    else:
        vsync_mode = DisplayServer.VSYNC_ADAPTIVE
        fps_limit = 0
