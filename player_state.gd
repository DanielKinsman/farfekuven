class_name PlayerState
extends Node
# Holds state for untrusted values to be synced to other peers from authority

var position: Vector3
var velocity: Vector3
var rotation: Vector3

# Which of the client's physics frames this state update is valid for.
# Only used for client side prediction updates, and os only applies to the
# client who controls the input for this player.
var input_physics_frame: int
var authority_physics_frame: int


func update(input_frame: int, position_val: Vector3, velocity_val: Vector3, rotation_val: Vector3) -> void:
    assert(is_multiplayer_authority(), "Only authority should update state")
    position = position_val
    velocity = velocity_val
    rotation = rotation_val
    input_physics_frame = input_frame
    authority_physics_frame = Engine.get_physics_frames()

    set_state.rpc(
        authority_physics_frame,
        input_physics_frame,
        position,
        velocity,
        rotation,
    )


@rpc(unreliable)
func set_state(
        authority_frame: int,
        input_frame: int,
        position_val: Vector3,
        velocity_val: Vector3,
        rotation_val: Vector3,
        ) -> void:

    authority_physics_frame = authority_frame
    input_physics_frame = input_frame
    position = position_val
    velocity = velocity_val
    rotation = rotation_val
