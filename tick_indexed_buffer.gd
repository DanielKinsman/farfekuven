class_name TickIndexedBuffer
extends Object
# Cicrular buffer of data indexed by frames/ticks


var data : Array
var max_frame := 0
var index_of_max_frame := 0


func _init(size: int = 120):
    data = Array()
    data.resize(size)
    data.fill(null)


func get_data(frame: int, default: Variant = null) -> Variant:
    var diff := max_frame - frame
    if diff < 0 or diff >= data.size():
        return default

    var value = data[index_of_max_frame - diff]  # python stule negative indices are fine
    if value == null:
        return default

    return value


func add(frame: int, datum):
    if frame >= max_frame:
        var diff := frame - max_frame
        for i in range(1, diff):
            var index := (index_of_max_frame + i) % data.size()
            data[index] = null

        index_of_max_frame = (index_of_max_frame + diff) % data.size()
        data[index_of_max_frame] = datum
        max_frame = frame
    elif max_frame - frame < data.size():
        var index := index_of_max_frame - (max_frame - frame)
        data[index] = datum  # python stule negative indices are fine


func get_min_frame() -> int:
    for i in range(max_frame - data.size(), max_frame, 1):
        if get_data(i) != null:
            return i

    return max_frame


static func tests(): # -> Error:
    var buff := TickIndexedBuffer.new(10)
    for i in range(10):
        assert(buff.get_data(i) == null)
        buff.add(i, i)

    assert(buff.get_data(0) == 0)

    var buff2 := TickIndexedBuffer.new(10)
    for i in range(11):
        buff2.add(i, i)

    assert(buff2.get_data(0) == null)
    assert(buff2.get_data(1) == 1)
    assert(buff2.get_data(10) == 10)

    var buff3 := TickIndexedBuffer.new(5)
    buff3.add(0, 0)
    buff3.add(2, 2)
    assert(buff3.get_data(0) == 0)
    assert(buff3.get_data(1) == null)
    assert(buff3.get_data(2) == 2)
    buff3.add(27, 27)
    assert(buff3.get_data(2) == null)
    assert(buff3.get_data(27) == 27)

    # test inserting earlier frame
    var buff4 := TickIndexedBuffer.new(5)
    buff4.add(36, 36)
    buff4.add(33, 33)
    assert(buff4.get_data(33) == 33)
    assert(buff4.get_data(34) == null)
    assert(buff4.get_data(35) == null)
    assert(buff4.get_data(36) == 36)

    return OK
