class_name Projectile
extends RigidDynamicBody3D

const PROJECTILE_VELOCITY := Vector3(0.0, 3.0, -10.0)


@onready var visual: Node3D = $Visual


func _ready():
    body_entered.connect(_body_entered)


func _process(delta: float):
    visual.position = visual.position.lerp(position, Engine.get_physics_interpolation_fraction())


func _body_entered(other: Node):
    if other is Projectile:
        return

    process_mode = Node.PROCESS_MODE_DISABLED
    visible = false


@rpc(reliable)
func replicate_fire(position_val: Vector3, velocity_val: Vector3):
    if process_mode != Node.PROCESS_MODE_DISABLED:
        return  # already fired on client side don't warp it

    position = position_val
    visual.position = position_val
    linear_velocity = velocity_val
    visible = get_parent().visible
    process_mode = get_parent().process_mode
