class_name PlayerInputs
extends Node
# Holds state for trusted values (inputs) to be synced to other peers

const JUMP = 0x1
const FIRE = 0x2

var motion := Vector3()
var actions := 0
var rotation := Vector3.ZERO

# Used for client side prediction only valid for the client owning this input.
var physics_frame: int = 0

# latest server frame seen on the client side when this input was sent
var authority_frame: int = 0

# Used on client side for prediction and on server for buffering a small amount inputs so we don't
# often end up with a tick with no input
@onready var input_buffer := TickIndexedBuffer.new(Engine.physics_ticks_per_second * 2)


func update(is_on_floor: bool, rotation_val: Vector3, authority_frame_val: int):
    assert(is_multiplayer_authority(), "Only authority should update state")
    assert(Engine.is_in_physics_frame(), "Input should only be processed once inside of a physics step")

    physics_frame = Engine.get_physics_frames()
    rotation = rotation_val

    var m = Vector3()
    if Input.is_action_pressed("move_forward"):
        m += Vector3.FORWARD

    if Input.is_action_pressed("move_backward"):
        m += Vector3.BACK

    if Input.is_action_pressed("move_left"):
        m += Vector3.LEFT

    if Input.is_action_pressed("move_right"):
        m += Vector3.RIGHT

    motion = m.rotated(Vector3.UP, rotation.y).normalized()

    actions = 0
    actions |= JUMP if is_on_floor and Input.is_action_pressed("jump") else 0
    actions |= FIRE if Input.is_action_just_pressed("fire") else 0

    input_buffer.add(physics_frame, [motion, actions, is_on_floor, rotation])

    set_inputs.rpc_id(
        get_parent().get_multiplayer_authority(),
        physics_frame,
        motion,
        actions,
        rotation,
        authority_frame_val,
    )


@rpc(unreliable)
func set_inputs(
        frame: int,
        motion_vector: Vector3,
        actions_taken: int,
        rotation_val: Vector3,
        authority_frame_val: int) -> void:

    if is_multiplayer_authority():
        # buffering and inputs already handled
        return

    motion_vector = motion_vector.normalized()  # don't trust client
    actions = actions_taken
    rotation_val.z = 0.0  # no roll... for now
    input_buffer.add(frame, [motion_vector, actions, rotation_val])

    if frame >= physics_frame:
        physics_frame = frame
        motion = motion_vector
        actions = actions_taken
        rotation = rotation_val
        authority_frame = authority_frame_val
