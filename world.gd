class_name World
extends Node3D

@onready var players := get_node("Players")
@onready var label_ping := get_node("LabelPing")


func _ready():
    if "--server" in OS.get_cmdline_args():
        State.host()
    else:
        State.join("localhost", Settings.server_port)
        get_tree().create_timer(1.0).timeout.connect(self_spawn)


func _process(_delta):
    for p in players.get_children():
        if not p.inputs.is_multiplayer_authority():
            continue

        var ping_ms = (p.input_latency_frames / float(Engine.physics_ticks_per_second)) * 1000.0
        label_ping.text = "ping %3d frames %4.1f ms" % [p.input_latency_frames, ping_ms]


func self_spawn():
    spawn_player.rpc_id(get_multiplayer_authority())


@rpc(any_peer, reliable)
func spawn_player():
    # TODO only allow one player per id
    get_node("PlayerSpawner").spawn(multiplayer.get_remote_sender_id())
