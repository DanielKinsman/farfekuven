class_name ProjectileSpawner
extends MultiplayerSpawner

const ProjectileScene := preload("res://projectile.tscn")


func _spawn_custom(data):
    var origin_transform: Transform3D = data[0]
    var player_velocity: Vector3 = data[1]
    var projectile: RigidDynamicBody3D = ProjectileScene.instantiate()
    projectile.position = origin_transform * Vector3.FORWARD * 1.0
    projectile.linear_velocity = player_velocity + (origin_transform.basis * Projectile.PROJECTILE_VELOCITY)
    return projectile
